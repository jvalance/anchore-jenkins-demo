#!/usr/bin/env python
import argparse
import re
import requests
import json
import csv

parser = argparse.ArgumentParser(description="Anchore Image Vuln Report Generator")

parser.add_argument('--u', metavar='user', default='admin', help='Anchore admin username (default=admin)')
parser.add_argument('--p', metavar='pass', default='foobar', help='Anchore admin password (default=foobar)')
parser.add_argument('--url', metavar='url', default='http://localhost:8228/v1/', help='Anchore Engine API Service endpoint URL (default=http://localhost:8228/v1/)')
parser.add_argument('--verify', metavar='verify', default=True, help='Accept self-signed certificates when using TLS/https for anchore endpoint')
parser.add_argument('--image', metavar='image', default='none', help='Full image tag to get vulnerability info. ex. docker.io/library/alpine:latest')

args = parser.parse_args()

endpoint_url = re.sub("\/+$", '', args.url)

# Get image digest

def getImageDigest():
    
    image_query = "/images"
    digest_request_url = endpoint_url + image_query
    payload = {'fulltag': args.image, 'history': 'false'}

    print(digest_request_url)

    try:
        r = requests.get(digest_request_url, auth=(args.u, args.p), params=payload, verify=args.verify)
        response_body = r.text

        if r.status_code == 200:
            # test that the response is valid JSON
            try:
                image_list = json.loads(response_body)
                image_digest = image_list[0]["imageDigest"]
                print(image_digest)
                return image_digest
            except:
                raise Exception("Got 200 response but is not valid JSON")

    except Exception as err:
        raise err

def getImageVulns():
    
    image_digest = getImageDigest()
    query_url = "/images/" + image_digest + "/vuln/all"
    request_url = endpoint_url + query_url

    try:
        r = requests.get(request_url, auth=(args.u, args.p), verify=args.verify)
        response_body = r.text

        if r.status_code == 200:
            # test that the response is valid json
            try:
                vuln_dict = json.loads(response_body)
                print(vuln_dict)
                return vuln_dict

            except:
                raise Exception("Got 200 response, but data isn't valid JSON")

    except Exception as err:
        # if any report fails, raise the error and failstop program
        raise err

getImageVulns()